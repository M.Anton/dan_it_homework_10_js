let tabs = document.querySelectorAll('.tabs-title');
let tabContent = document.querySelectorAll('.content-item');

function onTabClick(event) {

  tabs.forEach(function(tab) {
        tab.classList.remove('active');
    });

    event.target.classList.add('active');

    let tabIndex = Array.prototype.indexOf.call(tabs, event.target);

    tabContent.forEach(function(content) {
        content.style.display = 'none';
    });

    tabContent[tabIndex].style.display = 'block';
}

tabs.forEach(function(tab) {
    tab.addEventListener('click', onTabClick);
});